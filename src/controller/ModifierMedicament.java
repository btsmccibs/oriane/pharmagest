package controller;


import classes.MedicamentProduitEnInterne;
import classes.matiéredossage;
import classes.TypeMed;
import classes.ModePrise;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import sql.DBConnection;

import java.sql.SQLException;

public class ModifierMedicament {

    @FXML
    TextField nom;

    @FXML
    TextField Qte;

    @FXML
    TextField prix;

    @FXML
    ChoiceBox typemed;
    @FXML
    ChoiceBox modeprise;

    @FXML
    TextField qtemin;

    @FXML
    TextField remise;

    @FXML
    ToggleButton ord;

    int index;
    ObservableList<MedicamentProduitEnInterne> liste;
    MedicamentProduitEnInterne medicamentProdutInterne;
    int id;
    public void showinformation(int id, int index, ObservableList<MedicamentProduitEnInterne> list, MedicamentProduitEnInterne medicamentProdutInterne){
        this.index=index;
        this.liste=list;
        this.medicamentProdutInterne=medicamentProdutInterne;
        this.id=id;

        chargementDonnees(this.medicamentProdutInterne);
    }

    private void chargementDonnees(MedicamentProduitEnInterne medicamentProdutInterne) {
        ObservableList typesMedicament= FXCollections.observableArrayList("Antibiothique", "Antihistaminique", "analgersique");
        typemed.setItems(typesMedicament);
        ObservableList modesPriseMedicament= FXCollections.observableArrayList( "Orale", "Nasale", "Injectable", "ApplicationCutanée");
        modeprise.setItems(modesPriseMedicament);

        nom.setText(medicamentProdutInterne.getNomprod());
        qtemin.setText(Integer.toString(medicamentProdutInterne.getQtemin()));
        typemed.setValue(medicamentProdutInterne.getTypeMed().toString());
        modeprise.setValue(medicamentProdutInterne.getModePriseMed().toString());
        Qte.setText(Integer.toString(medicamentProdutInterne.getQte()));
        prix.setText(Integer.toString(medicamentProdutInterne.getPrix()));
        remise.setText(Double.toString(medicamentProdutInterne.getRemise()));
        ord.setSelected(medicamentProdutInterne.isOrdRequise());
    }


    @FXML
    TableView<matiéredossage> infomedinterne;
    @FXML
    TableColumn<matiéredossage, Integer> idmatiere;
    @FXML
    TableColumn<matiéredossage, String> nom2;
    @FXML
    TableColumn<matiéredossage,Integer> dossage;

    ObservableList<matiéredossage> listematieredossage;


    @FXML
    Button sauvegarder;
    public void sauvegarder() throws SQLException {
        DBConnection dbConnection =new DBConnection();
        String nom= this.nom.getText();
        int quantiteMin= Integer.valueOf(this.qtemin.getText());
        int quantite= Integer.valueOf(this.Qte.getText());
        int prix= Integer.valueOf(this.prix.getText());
        double remise= Double.valueOf(this.remise.getText());
        boolean ordonance = this.ord.isSelected();
        String typeMedicament = this.typemed.getValue().toString();
        String modePriseMedicament = this.modeprise.getValue().toString();
        //int quantite=this.medicamentProdutInterne.getQte()+Integer.valueOf(qteadd.getText());


        this.medicamentProdutInterne.setNomprod(nom);
        this.medicamentProdutInterne.setQte(quantite);
        this.medicamentProdutInterne.setQtemin(quantiteMin);
        this.medicamentProdutInterne.setPrix(prix);
        this.medicamentProdutInterne.setRemise(remise);
        this.medicamentProdutInterne.setOrdRequise(ordonance);
        this.medicamentProdutInterne.setTypeMed(TypeMed.valueOf(typeMedicament));
        this.medicamentProdutInterne.setModePriseMed(ModePrise.valueOf(modePriseMedicament));
        dbConnection.updateProduct(this.id,this.medicamentProdutInterne);
        dbConnection.updateMedicament(this.medicamentProdutInterne);
        liste.set(index,this.medicamentProdutInterne);

        Stage stage=(Stage) sauvegarder.getScene().getWindow();
        stage.close();
    }


    @FXML
    Button anuller;
    public void anuller(){
        chargementDonnees(this.medicamentProdutInterne);
    }
}
