package controller;

import classes.MedcinConvetionne;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sql.impmedcin;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class caissemedcin implements Initializable {


    static Label idclientlabel;

    public void showinformation(Label l){idclientlabel=l; }

    @FXML
    TextField searchmc;

    @FXML
    TableView<MedcinConvetionne> tablemedcin;
    @FXML
    TableColumn<MedcinConvetionne,Integer> idmedcin;
    @FXML
    TableColumn<MedcinConvetionne,String> nommedcin;
    @FXML
    TableColumn<MedcinConvetionne,String> prenommedcin;
    @FXML
    TableColumn<MedcinConvetionne,Integer> agemedcin;
    @FXML
    TableColumn<MedcinConvetionne,String> adresse;
    @FXML
    TableColumn<MedcinConvetionne,String> specialite;


    ObservableList<MedcinConvetionne> listmedcin;


    public void searchmedcin(){
        menu menu= new menu();
        menu.searchglobale(searchmc,listmedcin,tablemedcin);
    }

    int index;

    @FXML
    Button valide;
    public void affecter(){
        index=tablemedcin.getSelectionModel().getSelectedIndex();
        String id=idmedcin.getCellData(index).toString();
        idclientlabel.setText(id);


    }

    public void valider(){
        Stage stag=(Stage) valide.getScene().getWindow();
        stag.close();
    }



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        idmedcin.setCellValueFactory(new PropertyValueFactory<MedcinConvetionne, Integer>("idMed"));
        nommedcin.setCellValueFactory(new PropertyValueFactory<MedcinConvetionne, String>("nom"));
        prenommedcin.setCellValueFactory(new PropertyValueFactory<MedcinConvetionne, String>("prenom"));
        agemedcin.setCellValueFactory(new PropertyValueFactory<MedcinConvetionne, Integer>("age"));
      adresse.setCellValueFactory(new PropertyValueFactory<MedcinConvetionne, String>("adresse"));
        specialite.setCellValueFactory(new PropertyValueFactory<MedcinConvetionne, String>("specialite"));

        impmedcin impmedcin=new impmedcin();
        try {
            listmedcin = impmedcin.listmedcin();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        tablemedcin.setItems(listmedcin);

    }
}
