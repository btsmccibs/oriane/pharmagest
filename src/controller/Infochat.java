package controller;

import classes.MedicamentPréscrit;
import com.jfoenix.controls.JFXToggleButton;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import sql.impcommande;

import java.sql.SQLException;

public class Infochat {

    int index;

    public void showinformation(int i) {
        index = i;
    }

    @FXML
    TableView<MedicamentPréscrit> infoachat;
    @FXML
    TableColumn<MedicamentPréscrit, Integer> idprod;
    @FXML
    TableColumn<MedicamentPréscrit, Integer> qte;
    @FXML
    TableColumn<MedicamentPréscrit, String> nomprod;
    @FXML
    TableColumn<MedicamentPréscrit, Integer> prix;
    ObservableList<MedicamentPréscrit> listemedprescrit;

    @FXML
    JFXToggleButton jfxshow;

    public void showachat(){
        if(jfxshow.isSelected()){
            infoachat.setVisible(true);
            idprod.setCellValueFactory(new PropertyValueFactory<MedicamentPréscrit, Integer>("id"));
            nomprod.setCellValueFactory(new PropertyValueFactory<MedicamentPréscrit, String>("nomMed"));
            qte.setCellValueFactory(new PropertyValueFactory<MedicamentPréscrit, Integer>("qte"));
            prix.setCellValueFactory(new PropertyValueFactory<MedicamentPréscrit, Integer>("prix"));

            impcommande impcommande = new impcommande();
            try {
                listemedprescrit = impcommande.listemedprescritachat(index);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            infoachat.setItems(listemedprescrit);}
        else infoachat.setVisible(false);

    }
}
