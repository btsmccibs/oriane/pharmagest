package controller;


import classes.MedicamentPréscrit;
import com.jfoenix.controls.JFXToggleButton;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import sql.impcommande;

import java.sql.SQLException;


public class Infocommande {

    int index;

    public void showinformation(int i) {
        index = i;
    }

    @FXML
    TableView<MedicamentPréscrit> infocommande;
    @FXML
    TableColumn<MedicamentPréscrit, Integer> idmed;
    @FXML
    TableColumn<MedicamentPréscrit, Integer> qte;
    @FXML
    TableColumn<MedicamentPréscrit, String> nommed;
    @FXML
    TableColumn<MedicamentPréscrit, Integer> prix;
    ObservableList<MedicamentPréscrit> listemedprescrit;

    @FXML
    JFXToggleButton jfxshow;

        public void showcommande(){
        if(jfxshow.isSelected()){
            infocommande.setVisible(true);
        idmed.setCellValueFactory(new PropertyValueFactory<MedicamentPréscrit, Integer>("id"));
        nommed.setCellValueFactory(new PropertyValueFactory<MedicamentPréscrit, String>("nomMed"));
        qte.setCellValueFactory(new PropertyValueFactory<MedicamentPréscrit, Integer>("qte"));
        prix.setCellValueFactory(new PropertyValueFactory<MedicamentPréscrit, Integer>("prix"));

        impcommande impcommande = new impcommande();
        try {
            listemedprescrit = impcommande.listemedprescritcoomande(index);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        infocommande.setItems(listemedprescrit);}
        else infocommande.setVisible(false);

    }
}

