package controller;

import classes.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.stage.Stage;
import manipulation.outils;
import sql.impmed;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Modmed implements Initializable {
    ObservableList<MedicamentProduitEnInterne> listmedint;
    ObservableList<MedicamentProduitEnExterne> listmedext;

    public void showinformation(ObservableList<MedicamentProduitEnInterne> l1, ObservableList<MedicamentProduitEnExterne> l2){
        listmedint=l1;
        listmedext=l2;
    }

    @FXML
    TextField nom;

    @FXML
    ChoiceBox typemed;


    @FXML
    ToggleButton ord;



    @FXML
    Button suivant;

    public void valider(ActionEvent event) throws IOException {

        impmed impmed=new impmed();
        if(!nom.getText().isEmpty() &&!ord.getText().isEmpty()
         && !typemed.getValue().toString().isEmpty()){
            Medicament med=new Medicament(nom.getText(), TypeMed.valueOf(typemed.getValue().toString()),ord.isSelected());
            FXMLLoader loader=new FXMLLoader(getClass().getResource("/sample/detailmed.fxml"));
            Parent root= loader.load();

            Detailmed detailmed=loader.getController();
            detailmed.showinformation(listmedint,listmedext,med);
            Scene scene=new Scene(root);
            Stage stage=new Stage();
            stage.setScene(scene);
            stage.setTitle("Modifier un Medicament");
            stage.show();


            Stage stag=(Stage) suivant.getScene().getWindow();
            stag.close();

    }
        else {
            outils.showerroronmessage("erruer","remplire tout les champs");
        }
    }
    @FXML
    Button anuller;
    public void anuller(){
        Stage stage=(Stage) anuller.getScene().getWindow();
        stage.close();
    }



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList liste= FXCollections.observableArrayList("Antibiothique", "Antihistaminique", "analgersique");
        typemed.setItems(liste);
        ObservableList list= FXCollections.observableArrayList( "Orale", "Nasale", "Injectable", "ApplicationCutanée");
      ;
    }


    public void anullerTest() {
        System.out.println("text annuler");
    }


}
