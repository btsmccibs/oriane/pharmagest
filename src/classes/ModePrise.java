package classes;

public enum ModePrise {
    Orale,
    Nasale,
    Injectable,
    ApplicationCutanée
}
