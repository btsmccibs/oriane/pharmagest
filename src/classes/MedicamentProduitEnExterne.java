package classes;

public class MedicamentProduitEnExterne extends Medicament {
    private String NomFirme;

    public MedicamentProduitEnExterne(int idprod, int prix, int qte, String nomprod, classes.TypeMed typeMed, ModePrise modePriseMed, boolean ordRequise, int qtemin, double remise, String nomFirme) {
        super(idprod, prix, qte, nomprod, typeMed, modePriseMed, ordRequise, qtemin, remise);
        NomFirme = nomFirme;
    }

    public MedicamentProduitEnExterne(classes.TypeMed typeMed, ModePrise modePriseMed, boolean ordRequise, int qtemin, double remise, String nomFirme) {
        super(typeMed, modePriseMed, ordRequise, qtemin, remise);
        NomFirme = nomFirme;
    }

    public MedicamentProduitEnExterne(int idprod, int qte, String nomprod, double remise, String nomFirme) {
        super(idprod, qte, nomprod, remise);
        NomFirme = nomFirme;
    }

    public MedicamentProduitEnExterne(int idprod, int qte, String nomprod, double remise) {
        super(idprod, qte, nomprod, remise);
    }

    public MedicamentProduitEnExterne(int index, String nomprod, classes.TypeMed typeMed, boolean ordRequise, String text) {
        super(nomprod , typeMed , ordRequise);
    }

    public String getNomFirme() {
        return NomFirme;
    }

    public void setNomFirme(String nomFirme) {
        NomFirme = nomFirme;
    }
}
