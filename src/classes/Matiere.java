package classes;

public class Matiere {
    private String NomMatiére;
        private int idMatiére;

    public Matiere(String nomMatiére, int idMatiére) {
        NomMatiére = nomMatiére;
        this.idMatiére = idMatiére;
    }

    public Matiere(int idMatiére) {
        this.idMatiére = idMatiére;
    }

    public Matiere() {
    }

    public String getNomMatiére() {
        return NomMatiére;
    }

    public void setNomMatiére(String nomMatiére) {
        NomMatiére = nomMatiére;
    }

    public int getIdMatiére() {
        return idMatiére;
    }

    public void setIdMatiére(int idMatiére) {
        this.idMatiére = idMatiére;
    }
}
