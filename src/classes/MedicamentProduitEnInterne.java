package classes;

import java.util.ArrayList;

public class MedicamentProduitEnInterne extends Medicament {




    private ArrayList<matiéredossage> matiéredossages=new ArrayList<>();

    public MedicamentProduitEnInterne(int idprod, int prix, int qte, String nomprod, classes.TypeMed typeMed, ModePrise modePriseMed, boolean ordRequise, int qtemin, double remise) {
        super(idprod, prix, qte, nomprod, typeMed, modePriseMed, ordRequise, qtemin, remise);
    }

    public MedicamentProduitEnInterne(classes.TypeMed typeMed, ModePrise modePriseMed, boolean ordRequise, int qtemin, double remise) {
        super(typeMed, modePriseMed, ordRequise, qtemin, remise);
    }

    public MedicamentProduitEnInterne(int idprod, int qte, String nomprod, double remise) {
        super(idprod, qte, nomprod, remise);
    }

    public ArrayList<matiéredossage> getMatiéredossages() {
        return matiéredossages;
    }

    public void setMatiéredossages(ArrayList<matiéredossage> matiéredossages) {
        this.matiéredossages = matiéredossages;
    }

    public String toString(){

        return "Medicament "+this.getNomprod()+" "+this.getMatiéredossages()+" "+this.getQte();
    }
}
