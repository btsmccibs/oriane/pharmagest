package classes;

public  class Medicament extends Produit {

    private classes.TypeMed TypeMed;
    private ModePrise modePriseMed;
    private boolean OrdRequise;
    private int qtemin;
    private double remise;


    public Medicament(int idprod, int prix, int qte, String nomprod, classes.TypeMed typeMed, ModePrise modePriseMed, boolean ordRequise, int qtemin, double remise) {
        super(idprod, prix, qte, nomprod);
        TypeMed = typeMed;
        this.modePriseMed = modePriseMed;
        OrdRequise = ordRequise;
        this.qtemin = qtemin;
        this.remise=remise;
    }

    public Medicament(classes.TypeMed typeMed, ModePrise modePriseMed, boolean ordRequise, int qtemin, double remise) {
        TypeMed = typeMed;
        this.modePriseMed = modePriseMed;
        OrdRequise = ordRequise;
        this.qtemin = qtemin;
        this.remise=remise;

    }

    public Medicament(int idprod, int qte, String nomprod, double remise) {
        super(idprod, qte, nomprod);this.remise=remise;
    }

    public Medicament(String text, classes.TypeMed valueOf, boolean selected) {
    }


    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public classes.TypeMed getTypeMed() {
        return TypeMed;
    }

    public void setTypeMed(classes.TypeMed typeMed) {
        TypeMed = typeMed;
    }

    public ModePrise getModePriseMed() {
        return modePriseMed;
    }

    public void setModePriseMed(ModePrise modePriseMed) {
        this.modePriseMed = modePriseMed;
    }

    public boolean isOrdRequise() {
        return OrdRequise;
    }

    public void setOrdRequise(boolean ordRequise) {
        OrdRequise = ordRequise;
    }

    public int getQtemin() {
        return qtemin;
    }

    public void setQtemin(int qtemin) {
        this.qtemin = qtemin;
    }



}
