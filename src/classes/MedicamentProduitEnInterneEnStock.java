package classes;

import java.util.Date;

public class MedicamentProduitEnInterneEnStock extends MedicamentProduitEnInterne {

    private int Numlot;
    private Date dateExp;

    public MedicamentProduitEnInterneEnStock(int idprod, int prix, int qte, String nomprod, classes.TypeMed typeMed, ModePrise modePriseMed, boolean ordRequise, int qtemin, double remise, int numlot, Date dateExp) {
        super(idprod, prix, qte, nomprod, typeMed, modePriseMed, ordRequise, qtemin, remise);
        Numlot = numlot;
        this.dateExp = dateExp;
    }

    public MedicamentProduitEnInterneEnStock(classes.TypeMed typeMed, ModePrise modePriseMed, boolean ordRequise, int qtemin, double remise, int numlot, Date dateExp) {
        super(typeMed, modePriseMed, ordRequise, qtemin, remise);
        Numlot = numlot;
        this.dateExp = dateExp;
    }

    public MedicamentProduitEnInterneEnStock(int idprod, int qte, String nomprod, double remise, int numlot, Date dateExp) {
        super(idprod, qte, nomprod, remise);
        Numlot = numlot;
        this.dateExp = dateExp;
    }

    public int getNumlot() {
        return Numlot;
    }

    public void setNumlot(int numlot) {
        Numlot = numlot;
    }

    public Date getDateExp() {
        return dateExp;
    }

    public void setDateExp(Date dateExp) {
        this.dateExp = dateExp;
    }
}
