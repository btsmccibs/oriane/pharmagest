package classes;

public class ProduitParapharmacetique extends Produit {

    private classes.TypeProd TypeProd;


    public ProduitParapharmacetique(int idprod, int prix, int qte, String nomprod, classes.TypeProd typeProd) {
        super(idprod, prix, qte, nomprod);
        TypeProd = typeProd;
    }

    public ProduitParapharmacetique(classes.TypeProd typeProd) {
        TypeProd = typeProd;
    }

    public classes.TypeProd getTypeProd() {
        return TypeProd;
    }

    public void setTypeProd(classes.TypeProd typeProd) {
        TypeProd = typeProd;
    }
}
