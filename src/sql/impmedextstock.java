package sql;


import classes.MedicamentProduitEnExterneEnStock;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sample.BD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class impmedextstock implements imedextstock {
    private static Connection conn = BD.connect();

    @Override
    public boolean ajoutermedextstock(int numlot, LocalDate dateexp) {
        try {

            PreparedStatement ps = conn.prepareStatement("insert into medexternestock values (last_insert_id()," + numlot +",'"+dateexp+ "')");
            return ps.execute();


        } catch (Exception e) {
            System.out.println(e);
            try {

            } catch (Exception ex) {

            }

        }

        return true;
    }

    public boolean validermedextstock(int numlot, LocalDate dateexp) {
        try {

            PreparedStatement ps = conn.prepareStatement("insert into medexternestock values (last_insert_id()," + numlot +",'"+dateexp+ "')");
            return ps.execute();


        } catch (Exception e) {
            System.out.println(e);
            try {

            } catch (Exception ex) {

            }

        }

        return true;
    }
    @Override
    public ObservableList<MedicamentProduitEnExterneEnStock> listmedextstock() throws SQLException {
        ObservableList<MedicamentProduitEnExterneEnStock> list = FXCollections.observableArrayList();
        PreparedStatement ps = conn.prepareStatement("select * from produit inner join medexternestock on idprod=idmedexternestock inner join medicament on idmedexternestock=idmed ");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {

            list.add(new MedicamentProduitEnExterneEnStock(rs.getInt("idprod"),rs.getInt("Qte"),
                    rs.getString("nom"),rs.getDouble("remise"),rs.getInt("numlot"),rs.getDate("dateexp")));

        }

        return list;
    }
    @Override
    public LocalDate getdateexp(int id) throws SQLException {
        PreparedStatement ps=conn.prepareStatement("select dateexp from medexternestock where idmedexternestock="+id);
        ResultSet rs=ps.executeQuery();
        if(rs.next()){return rs.getDate("dateexp").toLocalDate();
        }
        else return null;
    }
}
