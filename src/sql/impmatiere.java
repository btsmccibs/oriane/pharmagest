package sql;

import classes.Matiere;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import manipulation.outils;
import sample.BD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class impmatiere implements imatiere{
    private static Connection conn = BD.connect();

    @Override
    public boolean ajoutermatiere(String nom) {
        try {
            PreparedStatement ps = conn.prepareStatement("insert into matiere(nom) values('"+ nom +"')");

            return ps.execute();

        } catch (Exception e) {
            outils.showerroronmessage("error","matiere existe deja");
            try {


            } catch (Exception ex) {

            }

        }

        return true;
    }

    @Override
    public ObservableList<Matiere> listematiere() throws SQLException {
        ObservableList<Matiere> list= FXCollections.observableArrayList();
        PreparedStatement ps=conn.prepareStatement("select *from matiere");
        ResultSet rs=ps.executeQuery();
        while(rs.next()){
            list.add(new Matiere(rs.getString("nom"),rs.getInt("idmatiere")));
        }
        return list;
    }
}
