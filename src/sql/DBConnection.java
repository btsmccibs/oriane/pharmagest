package sql;

import classes.MedicamentProduitEnInterne;
import classes.Produit;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sample.BD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBConnection implements iprod{
    private static Connection conn = BD.connect();

    @Override
    public boolean updateget(int id, int qte) throws SQLException {

        try {
            int Qte;
            PreparedStatement ps=conn.prepareStatement("select Qte from produit where idprod="+id);
            ResultSet rs=ps.executeQuery();
            if(rs.next()){Qte= qte+rs.getInt("Qte");
            }
            else Qte=qte;
            System.out.println(Qte);
            ps = conn.prepareStatement("update produit set Qte="+Qte+" where idprod="+id);

            return !ps.execute();


        } catch (SQLException e) {

            System.out.println(e);
        }
        finally {
            try {

            } catch (Exception ex) {

            }
        }
        return false;
    }

    @Override
    public boolean updateProduct(int id, MedicamentProduitEnInterne medicamentProduitEnInterne) throws SQLException {
            int Qte;
            PreparedStatement ps=conn.prepareStatement("select Qte from produit where idprod="+id);
            ResultSet rs=ps.executeQuery();
            if(rs.next()){Qte= medicamentProduitEnInterne.getQte()+rs.getInt("Qte");
            }
            else Qte=medicamentProduitEnInterne.getQte();
            String updateProduit = "UPDATE produit SET nom = ?, qte = ?, prix = ? WHERE idprod = ?";
           // ps = conn.prepareStatement("update produit set Qte="+Qte+" where idprod="+id);
            PreparedStatement statement = conn.prepareStatement(updateProduit);
            // Paramètres de la requête
            statement.setString(1, medicamentProduitEnInterne.getNomprod());
            statement.setInt(2, medicamentProduitEnInterne.getQte());
            statement.setDouble(3, medicamentProduitEnInterne.getPrix());
            statement.setInt(4, medicamentProduitEnInterne.getIdprod());

            // Ajout de la mise à jour dans le batch
            statement.addBatch();
            // Exécution du batch
            int[] rowsAffected = statement.executeBatch();
            // Fermeture du statement
            statement.close();
            // Vérification du nombre de lignes affectées
            return rowsAffected.length > 0 && rowsAffected[0] > 0;
    }

    @Override
    public boolean updateMedicament(MedicamentProduitEnInterne medicamentProduitEnInterne) throws SQLException {
        String updateMedicament = "UPDATE medicament SET  qtemin = ?, remise = ?, ordrequise = ?, typemed = ?, modeprise = ? WHERE idmed = ?";
        PreparedStatement statement = conn.prepareStatement(updateMedicament);
        // Paramètres de la requête
        statement.setInt(1, medicamentProduitEnInterne.getQtemin());
        statement.setDouble(2, medicamentProduitEnInterne.getRemise());
        int intValue = medicamentProduitEnInterne.isOrdRequise() ? 1 : 0; // Conversion valeur boolean en int
        statement.setInt(3, intValue);
        statement.setString(4, medicamentProduitEnInterne.getTypeMed().toString());
        statement.setString(5, medicamentProduitEnInterne.getModePriseMed().toString());
        statement.setInt(6, medicamentProduitEnInterne.getIdprod());

        // Ajout de la mise à jour dans le batch
        statement.addBatch();
        // Exécution du batch
        int[] rowsAffected = statement.executeBatch();
        // Fermeture du statement
        statement.close();
        // Vérification du nombre de lignes affectées
        return rowsAffected.length > 0 && rowsAffected[0] > 0;
    }

    @Override
    public ObservableList<Produit> listeprod() throws SQLException {

        ObservableList<Produit> list= FXCollections.observableArrayList();
        PreparedStatement ps=conn.prepareStatement("select * from commande where idclient=0");
        ResultSet rs=ps.executeQuery();
        while(rs.next()){
            list.add(new Produit(rs.getInt("idprod"),rs.getInt("Qte"),rs.getString("nom"),rs.getInt("prix")));
        }
        return list;
    }

    @Override
    public int getqtemin(int id) throws SQLException {
        PreparedStatement ps=conn.prepareStatement("select qtemin from medicament where idmed="+id);
        ResultSet rs=ps.executeQuery();
        if(rs.next()){return rs.getInt("qtemin");
        }
        else return 0;
    }

    @Override
    public int getqte(int id) throws SQLException {
        PreparedStatement ps=conn.prepareStatement("select Qte from produit where idprod="+id);
        ResultSet rs=ps.executeQuery();
        if(rs.next()){return rs.getInt("Qte");
        }
        else return 0;
    }

}

