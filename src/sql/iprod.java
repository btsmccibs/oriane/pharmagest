package sql;

import classes.MedicamentProduitEnInterne;
import classes.Produit;
import javafx.collections.ObservableList;

import java.sql.SQLException;

public interface iprod {
    public boolean updateget(int id, int qte) throws SQLException;
    public boolean updateProduct(int id, MedicamentProduitEnInterne medicamentProduitEnInterne) throws SQLException;
    public boolean updateMedicament(MedicamentProduitEnInterne medicamentProduitEnInterne) throws SQLException;
    ObservableList<Produit> listeprod() throws SQLException;
    public int getqtemin(int id) throws SQLException;
    public int getqte(int id) throws SQLException;
}
