package sql;

import classes.MedicamentProduitEnExterne;
import classes.ModePrise;
import classes.TypeMed;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sample.BD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class impmedext implements imedext {
    private static Connection conn = BD.connect();

    @Override
    public boolean ajoutermedext(String nomfirme) {
        try {

            PreparedStatement ps = conn.prepareStatement("insert into medexterne values (last_insert_id(),'" + nomfirme + "')");
            return ps.execute();


        } catch (Exception e) {
            try {

            } catch (Exception ex) {

            }

        }

        return true;
    }
    public boolean validermedext(String nomfirme) {
        try {

            PreparedStatement ps = conn.prepareStatement("insert into medexterne values (last_insert_id(),'" + nomfirme + "')");
            return ps.execute();


        } catch (Exception e) {
            try {

            } catch (Exception ex) {

            }

        }

        return true;
    }

    @Override
    public ObservableList<MedicamentProduitEnExterne> listmedext() throws SQLException {
        ObservableList<MedicamentProduitEnExterne> list = FXCollections.observableArrayList();
        PreparedStatement ps = conn.prepareStatement("select *from medicament inner join produit on idmed=idprod " +
                "inner join medexterne on idmed=idmedexterne");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {

            list.add(new MedicamentProduitEnExterne(rs.getInt("idprod"),rs.getInt("prix"),
                    rs.getInt("Qte"),rs.getString("nom"), TypeMed.valueOf(rs.getString("typemed")),
                    ModePrise.valueOf(rs.getString("modeprise")),rs.getBoolean("ordrequise"),
                    rs.getInt("qtemin"),rs.getDouble("remise"),rs.getString("nomfirme")));

        }

        return list;
    }

}
