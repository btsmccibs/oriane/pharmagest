package sql;


import classes.MedicamentProduitEnExterne;
import javafx.collections.ObservableList;

import java.sql.SQLException;

public interface imedext {
    public boolean ajoutermedext(String nomfirme);
    public ObservableList<MedicamentProduitEnExterne> listmedext() throws SQLException;
}
