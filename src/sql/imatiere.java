package sql;

import classes.Matiere;
import javafx.collections.ObservableList;

import java.sql.SQLException;

public interface imatiere {
    public boolean ajoutermatiere(String nom);
    public ObservableList<Matiere> listematiere() throws SQLException;
}
