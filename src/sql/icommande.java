package sql;

import classes.Commande;
import classes.MedicamentPréscrit;
import javafx.collections.ObservableList;

import java.sql.SQLException;
import java.time.LocalDate;

public interface icommande {

    public ObservableList<MedicamentPréscrit> listemedprescritcoomande(int idcommande) throws SQLException;
    public ObservableList<MedicamentPréscrit> listemedprescritachat(int idcommande) throws SQLException;
    public boolean ajoutermedprescrit(int idcommande, int idmedprescrit, int qte, LocalDate dure, String nom);
    public ObservableList<Commande> listcommandeclient(int idclient) throws SQLException;
    public ObservableList<Commande> listcommande() throws SQLException;
}
